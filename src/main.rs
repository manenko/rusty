extern crate crossbeam;
extern crate gdk_pixbuf;
extern crate gio;
extern crate gtk;
extern crate id3;
extern crate pulse_simple;
extern crate simplemad;

mod mp3;
mod player;
mod playlist;
mod toolbar;

use std::collections::HashMap;
use std::env;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use gio::{ApplicationExt, ApplicationExtManual, ApplicationFlags};
use gtk::{
    Adjustment, AdjustmentExt, Application, ApplicationWindow, ContainerExt, Continue,
    GtkWindowExt, Image, ImageExt, Orientation, Scale, ScaleExt, WidgetExt,
};

use playlist::Playlist;
use toolbar::MusicToolbar;

struct State {
    current_time: u64,
    durations: HashMap<String, u64>,
    stopped: bool,
}

struct App {
    adjustment: Adjustment,
    cover: Image,
    playlist: Rc<Playlist>,
    state: Arc<Mutex<State>>,
    toolbar: MusicToolbar,
    window: ApplicationWindow,
}

impl App {
    pub fn new(application: Application) -> Self {
        let window = ApplicationWindow::new(&application);
        let vbox = gtk::Box::new(Orientation::Vertical, 0);
        window.add(&vbox);

        let toolbar = MusicToolbar::new();
        vbox.add(toolbar.toolbar());

        let state = Arc::new(Mutex::new(State {
            current_time: 0,
            durations: HashMap::new(),
            stopped: true,
        }));

        let playlist = Rc::new(Playlist::new(state.clone()));
        vbox.add(playlist.view());

        let cover = Image::new();
        cover.set_from_file("cover.jpg");
        vbox.add(&cover);

        let adjustment = Adjustment::new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let scale = Scale::new(Orientation::Horizontal, &adjustment);
        scale.set_draw_value(false);
        vbox.add(&scale);

        window.set_title("Rusty");
        window.show_all();

        let app = App {
            adjustment,
            cover,
            playlist,
            state,
            toolbar,
            window,
        };

        app.connect_events();
        app.connect_toolbar_events();

        app
    }

    fn connect_events(&self) {
        let playlist = self.playlist.clone();
        let adjustment = self.adjustment.clone();
        let state = self.state.clone();

        gtk::timeout_add(100, move || {
            let state = state.lock().unwrap();
            if let Some(path) = playlist.path() {
                if let Some(&duration) = state.durations.get(&path) {
                    adjustment.set_upper(duration as f64);
                }
            }

            if state.stopped {
                // TODO: Set icon to PLAY_STOCK
            } else {
                // TODO: Set icon to PAUSE_STOCK
            }

            adjustment.set_value(state.current_time as f64);

            Continue(true)
        });
    }
}

fn main() {
    let application = Application::new("com.manenko.rusty", ApplicationFlags::empty())
        .expect("Application initialization failed.");

    application.connect_startup(|application| {
        let _app = App::new(application.clone());
    });

    application.connect_activate(|_| {});
    application.run(&env::args().collect::<Vec<_>>());
}

fn to_milliseconds(duration: Duration) -> u64 {
    duration.as_secs() * 1000 + duration.subsec_nanos() as u64 / 1_000_000
}
