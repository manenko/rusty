//
// src/toolbar.rs
//
// Created by Oleksandr Manenko on 17-08-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

use std::path::PathBuf;

use gtk::{
    ApplicationWindow, ContainerExt, DialogExt, FileChooserAction, FileChooserDialog,
    FileChooserExt, FileFilter, FileFilterExt, Image, ImageExt, ResponseType, SeparatorToolItem,
    ToolButton, ToolButtonExt, Toolbar, WidgetExt,
};

use playlist::Playlist;
use App;

const PLAY_STOCK: &str = "gtk-media-play";
const PAUSE_STOCK: &str = "gtk-media-pause";
const STOP_STOCK: &str = "gtk-stop";
const OPEN_STOCK: &str = "gtk-open";
const NEXT_STOCK: &str = "gtk-media-next";
const PREVIOUS_STOCK: &str = "gtk-media-previous";
const REMOVE_STOCK: &str = "gtk-remove";
const QUIT_STOCK: &str = "gtk-quit";

pub struct MusicToolbar {
    open_button: ToolButton,
    play_button: ToolButton,
    stop_button: ToolButton,
    next_button: ToolButton,
    previous_button: ToolButton,
    quit_button: ToolButton,
    remove_button: ToolButton,
    toolbar: Toolbar,
}

impl MusicToolbar {
    pub fn new() -> Self {
        let toolbar = Toolbar::new();

        let open_button = ToolButton::new_from_stock(OPEN_STOCK);
        toolbar.add(&open_button);
        toolbar.add(&SeparatorToolItem::new());

        let play_button = ToolButton::new_from_stock(PLAY_STOCK);
        toolbar.add(&play_button);

        let stop_button = ToolButton::new_from_stock(STOP_STOCK);
        toolbar.add(&stop_button);

        let previous_button = ToolButton::new_from_stock(PREVIOUS_STOCK);
        toolbar.add(&previous_button);

        let next_button = ToolButton::new_from_stock(NEXT_STOCK);
        toolbar.add(&next_button);

        toolbar.add(&SeparatorToolItem::new());

        let remove_button = ToolButton::new_from_stock(REMOVE_STOCK);
        toolbar.add(&remove_button);

        toolbar.add(&SeparatorToolItem::new());

        let quit_button = ToolButton::new_from_stock(QUIT_STOCK);
        toolbar.add(&quit_button);

        MusicToolbar {
            open_button,
            play_button,
            stop_button,
            next_button,
            previous_button,
            remove_button,
            quit_button,
            toolbar,
        }
    }

    pub fn toolbar(&self) -> &Toolbar {
        &self.toolbar
    }
}

impl App {
    pub fn connect_toolbar_events(&self) {
        let window = self.window.clone();
        self.toolbar.quit_button.connect_clicked(move |_| {
            window.destroy();
        });

        let parent = self.window.clone();
        let playlist = self.playlist.clone();
        self.toolbar.open_button.connect_clicked(move |_| {
            let file = show_open_dialog(&parent);
            if let Some(file) = file {
                playlist.add(&file);
            }
        });

        let playlist = self.playlist.clone();
        self.toolbar.remove_button.connect_clicked(move |_| {
            playlist.remove_selection();
        });

        let play_button = self.toolbar.play_button.clone();
        let playlist = self.playlist.clone();
        let cover = self.cover.clone();
        let state = self.state.clone();
        self.toolbar.play_button.connect_clicked(move |_| {
            if state.lock().unwrap().stopped {
                if playlist.play() {
                    play_button.set_stock_id(PAUSE_STOCK);
                    set_cover(&cover, &playlist);
                }
            } else {
                playlist.pause();
                play_button.set_stock_id(PLAY_STOCK);
            }
        });

        let playlist = self.playlist.clone();
        let cover = self.cover.clone();
        let play_button = self.toolbar.play_button.clone();
        self.toolbar.stop_button.connect_clicked(move |_| {
            playlist.stop();
            cover.hide();
            play_button.set_stock_id(PLAY_STOCK);
        });

        let playlist = self.playlist.clone();
        let play_button = self.toolbar.play_button.clone();
        let cover = self.cover.clone();
        self.toolbar.next_button.connect_clicked(move |_| {
            if playlist.next() {
                play_button.set_stock_id(PAUSE_STOCK);
                set_cover(&cover, &playlist);
            }
        });

        let playlist = self.playlist.clone();
        let play_button = self.toolbar.play_button.clone();
        let cover = self.cover.clone();
        self.toolbar.previous_button.connect_clicked(move |_| {
            if playlist.previous() {
                play_button.set_stock_id(PAUSE_STOCK);
                set_cover(&cover, &playlist);
            }
        });
    }
}

fn show_open_dialog(parent: &ApplicationWindow) -> Option<PathBuf> {
    let dialog = FileChooserDialog::with_buttons(
        Some("Select an MP3 audio file"),
        Some(parent),
        FileChooserAction::Open,
        &[
            ("_Cancel", ResponseType::Cancel),
            ("_Open", ResponseType::Accept),
        ],
    );

    let filter = FileFilter::new();
    filter.add_mime_type("audio/mp3");
    filter.set_name("MP3 audio file");

    dialog.add_filter(&filter);

    let result = dialog.run();

    let mut file = None;
    if result == ResponseType::Accept.into() {
        file = dialog.get_filename();
    }

    dialog.destroy();

    file
}

fn set_cover(cover: &Image, playlist: &Playlist) {
    cover.set_from_pixbuf(playlist.pixbuf().as_ref());
    cover.show();
}
