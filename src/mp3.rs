//
// src/mp3.rs
//
// Created by Oleksandr Manenko on 21-08-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

use std::io::{Read, Seek, SeekFrom};
use std::time::Duration;

use simplemad;
use to_milliseconds;

pub struct Mp3Decoder<R>
where
    R: Read,
{
    reader: simplemad::Decoder<R>,
    current_frame: simplemad::Frame,
    current_frame_channel: usize,
    current_frame_sample_pos: usize,
    current_time: u64,
}

impl<R> Mp3Decoder<R>
where
    R: Read + Seek,
{
    pub fn new(mut data: R) -> Result<Mp3Decoder<R>, R> {
        if !is_mp3(data.by_ref()) {
            return Err(data);
        }

        let mut reader = simplemad::Decoder::decode(data).unwrap();
        let current_frame = next_frame(&mut reader);
        let current_time = to_milliseconds(current_frame.duration);

        Ok(Mp3Decoder {
            reader,
            current_frame,
            current_frame_channel: 0,
            current_frame_sample_pos: 0,
            current_time,
        })
    }

    pub fn current_time(&self) -> u64 {
        self.current_time
    }

    pub fn samples_rate(&self) -> u32 {
        self.current_frame.sample_rate
    }

    pub fn compute_duration(mut data: R) -> Option<Duration> {
        if !is_mp3(data.by_ref()) {
            return None;
        }

        let decoder = simplemad::Decoder::decode_headers(data).unwrap();

        Some(
            decoder
                .filter_map(|frame| match frame {
                    Ok(frame) => Some(frame.duration),
                    Err(_) => None,
                })
                .sum(),
        )
    }
}

impl<R> Iterator for Mp3Decoder<R>
where
    R: Read,
{
    type Item = i32;

    fn next(&mut self) -> Option<i32> {
        next_sample(self)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.current_frame.samples[0].len(), None)
    }
}

fn is_mp3<R>(mut data: R) -> bool
where
    R: Read + Seek,
{
    let stream_pos = data.seek(SeekFrom::Current(0)).unwrap();
    let is_mp3 = simplemad::Decoder::decode(data.by_ref()).is_ok();
    data.seek(SeekFrom::Start(stream_pos)).unwrap();

    is_mp3
}

fn default_frame() -> simplemad::Frame {
    simplemad::Frame {
        bit_rate: 0,
        layer: Default::default(),
        mode: Default::default(),
        sample_rate: 44100,
        samples: vec![Vec::new()],
        position: Duration::from_secs(0),
        duration: Duration::from_secs(0),
    }
}

fn next_frame<R: Read>(decoder: &mut simplemad::Decoder<R>) -> simplemad::Frame {
    decoder
        .filter_map(|f| f.ok())
        .next()
        .unwrap_or_else(default_frame)
}

fn is_any_sample_available(frame: &simplemad::Frame) -> bool {
    !(frame.samples.is_empty() || frame.samples[0].is_empty())
}

fn next_sample<R>(decoder: &mut Mp3Decoder<R>) -> Option<i32>
where
    R: Read,
{
    if !is_any_sample_available(&decoder.current_frame) {
        return None;
    }

    let channel = decoder.current_frame_channel;
    let sample_pos = decoder.current_frame_sample_pos;

    let sample = decoder.current_frame.samples[channel][sample_pos].to_i32();

    decoder.current_frame_channel += 1;

    if decoder.current_frame_channel < decoder.current_frame.samples.len() {
        return Some(sample);
    }

    decoder.current_frame_channel = 0;
    decoder.current_frame_sample_pos += 1;

    if decoder.current_frame_sample_pos < decoder.current_frame.samples[0].len() {
        return Some(sample);
    }

    decoder.current_frame = next_frame(&mut decoder.reader);
    decoder.current_frame_channel = 0;
    decoder.current_frame_sample_pos = 0;
    decoder.current_time += to_milliseconds(decoder.current_frame.duration);

    Some(sample)
}
